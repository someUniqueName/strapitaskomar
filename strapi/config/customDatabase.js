const mysql = require('mysql'),
     util = require('util'),
     {database} = require('../envConfing');

//this is our custom db instance 
//needed to do this since i wanted to access db and write my own SP's which is handy when it comes to 
//complex queries 
const pool  = mysql.createPool({
  connectionLimit : 10,
  host            : database.host,
  user            : database.user,
  password        : database.password,
  database        : database.main,
  port            : database.port
});

// export pool connection and warp it in a promise
pool.query = util.promisify(pool.query)


module.exports = pool
