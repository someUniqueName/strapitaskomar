const {database} = require('../envConfing');
module.exports = ({
  env
}) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', database.host),
        port: env.int('DATABASE_PORT', database.port),
        database: env('DATABASE_NAME', database.main),
        username: env('DATABASE_USERNAME', database.user),
        password: env('DATABASE_PASSWORD', database.password),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {
        "pool": {
          "min": 0,
          "max": 15,
          "idleTimeoutMillis": 30000,
          "createTimeoutMillis": 30000,
          "acquireTimeoutMillis": 30000
        }
      }
    },
  },
});



