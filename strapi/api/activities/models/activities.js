'use strict';

const mail = require('../../../services/nodemail');
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#life-cycle-callbacks)
 * to customize this model
 */

module.exports = {
    // life cycle hook to send mail after a successful insert need to use try/catch  
    lifecycles: {
        async afterCreate(result, data) {
            await mail();
        },
      },
};
