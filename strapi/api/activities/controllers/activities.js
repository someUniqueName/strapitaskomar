'use strict';
const mysqlPool = require('../../../config/customDatabase');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  // our custom controller to handle custom api's  
  discount : async ctx =>  {
    try {
    // get Discount from body 
    const { Discount } = ctx.request.body
    // call procedure and preform required task
    const result = await mysqlPool.query(`call discount_activities(${Discount})`);
    ctx.send({message : "records updated."})
    }catch(error){
      ctx.send('server error 500.')
    }
  },
};
