
//require nodemailer and confgire it 
const nodemailer = require("nodemailer"),
  config = require('../envConfing.js'),
  transporter = nodemailer.createTransport({
    service: config.emailService.agent,
    auth: {
      user: config.emailService.email,
      pass: config.emailService.password
    }
  });


// function to prepare sending mail
async function sendMail() {
    let message = {
      from: config.emailService.email,
      to: 'info@mallorcard.es',
      subject: 'one activity added',
      html: '<p>one activity added ! test</p>'
    }
    return sendEmail(message);
}

// warp mail object in promise 
function sendEmail(message) {
    return new Promise((resolve, reject) => {
      transporter.sendMail(message, (error) => {
        if (error) {
          reject(error);
        }
        resolve();
        transporter.close();
      });
    })
  }

module.exports = sendMail


