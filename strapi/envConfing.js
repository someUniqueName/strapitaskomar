if (process.env.NODE_ENV !== 'production') {
    //if production then skip this since this is not the best way to do it in production
    require('dotenv').config({path: __dirname + '/env/.env.' + process.env.NODE_ENV});
  }
  
  // this is our config file here we declare our environment variables to use across application
  const config = {
    database : {
      host : process.env.dataBaseHost,
      main :process.env.dataBaseNameMain,
      port : process.env.dataBasePort,
      user : process.env.dataBaseUser,
      password : process.env.dataBasePassword,
    },
    emailService : {
      agent : process.env.emailServiceAgent,
      email : process.env.emailUserEmail,
      password : process.env.emailUserPassword
    }
  };
  
  
  module.exports = config;